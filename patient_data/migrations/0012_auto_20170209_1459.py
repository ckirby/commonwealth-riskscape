# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0011_auto_20170209_1426'),
    ]

    operations = [
        migrations.AlterField(
            model_name='riskscapedata',
            name='birth_cohort',
            field=models.PositiveSmallIntegerField(verbose_name='Birth Cohort', null=True, choices=[(0, 'Before 1945'), (1, '1945-1965'), (2, 'After 1965')]),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='birth_cohort',
            field=models.PositiveSmallIntegerField(verbose_name='Birth Cohort', null=True, choices=[(0, 'Before 1945'), (1, '1945-1965'), (2, 'After 1965')]),
        ),
    ]
