# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0009_auto_20170208_1727'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='uploaddata',
            name='attempt_failed',
        ),
        migrations.AddField(
            model_name='uploaddata',
            name='status',
            field=models.IntegerField(choices=[(0, 'Pending'), (1, 'Running'), (2, 'Success'), (3, 'Failure')], default=2),
            preserve_default=False,
        ),
    ]
