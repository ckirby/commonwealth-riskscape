# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0002_auto_20161117_0924'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='riskscapedata',
            name='zip_code',
        ),
        migrations.RemoveField(
            model_name='trimtrackerdata',
            name='zip_code',
        ),
    ]
