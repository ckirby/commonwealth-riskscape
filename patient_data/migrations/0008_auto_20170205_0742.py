# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0007_auto_20170124_0903'),
    ]

    operations = [
        migrations.AddField(
            model_name='uploaddata',
            name='attempt_failed',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='riskscapedata',
            name='curr_flu_vac',
            field=models.PositiveSmallIntegerField(null=True, blank=True, choices=[(1, 'Yes'), (0, 'No')], verbose_name='Seasonal Flu Vaccine'),
        ),
        migrations.AlterField(
            model_name='riskscapedata',
            name='gdm',
            field=models.PositiveSmallIntegerField(null=True, blank=True, choices=[(1, 'Yes'), (0, 'No')], verbose_name='GDM'),
        ),
        migrations.AlterField(
            model_name='riskscapedata',
            name='ili_current',
            field=models.PositiveSmallIntegerField(null=True, blank=True, choices=[(1, 'Yes'), (0, 'No')], verbose_name='Seasonal ILI'),
        ),
        migrations.AlterField(
            model_name='riskscapedata',
            name='lyme',
            field=models.PositiveSmallIntegerField(null=True, blank=True, choices=[(1, 'Yes'), (0, 'No')], verbose_name='Lyme (this year)'),
        ),
        migrations.AlterField(
            model_name='riskscapedata',
            name='opioid_benzo_concurrent',
            field=models.PositiveSmallIntegerField(null=True, blank=True, choices=[(1, 'Yes'), (0, 'No')], verbose_name='Benzo and Opioid'),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='curr_flu_vac',
            field=models.PositiveSmallIntegerField(null=True, blank=True, choices=[(1, 'Yes'), (0, 'No')], verbose_name='Seasonal Flu Vaccine'),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='gdm',
            field=models.PositiveSmallIntegerField(null=True, blank=True, choices=[(1, 'Yes'), (0, 'No')], verbose_name='GDM'),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='ili_current',
            field=models.PositiveSmallIntegerField(null=True, blank=True, choices=[(1, 'Yes'), (0, 'No')], verbose_name='Seasonal ILI'),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='lyme',
            field=models.PositiveSmallIntegerField(null=True, blank=True, choices=[(1, 'Yes'), (0, 'No')], verbose_name='Lyme (this year)'),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='opioid_benzo_concurrent',
            field=models.PositiveSmallIntegerField(null=True, blank=True, choices=[(1, 'Yes'), (0, 'No')], verbose_name='Benzo and Opioid'),
        ),
    ]
