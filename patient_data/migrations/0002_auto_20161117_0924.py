# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='riskscapedata',
            name='tdap_preg',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='TDaP Vaccine in Pregnancy', choices=[(1, 'Yes'), (0, 'No')]),
        ),
        migrations.AddField(
            model_name='trimtrackerdata',
            name='tdap_preg',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='TDaP Vaccine in Pregnancy', choices=[(1, 'Yes'), (0, 'No')]),
        ),
        migrations.AlterField(
            model_name='riskscapedata',
            name='a1c',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='A1C', choices=[(0, 'Not measured'), (1, '0-5.6'), (2, '5.7-6.4'), (3, '6.5-8.0'), (4, '&gt;8.0')]),
        ),
        migrations.AlterField(
            model_name='riskscapedata',
            name='bmi',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='BMI', choices=[(0, 'Not measured'), (1, 'Normal (BMI&nbsp;&lt;25)'), (2, 'Overweight (BMI&nbsp;25-&lt;30)'), (3, 'Obese (BMI&nbsp;&gt;30.0)')]),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='a1c',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='A1C', choices=[(0, 'Not measured'), (1, '0-5.6'), (2, '5.7-6.4'), (3, '6.5-8.0'), (4, '&gt;8.0')]),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='bmi',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='BMI', choices=[(0, 'Not measured'), (1, 'Normal (BMI&nbsp;&lt;25)'), (2, 'Overweight (BMI&nbsp;25-&lt;30)'), (3, 'Obese (BMI&nbsp;&gt;30.0)')]),
        ),
    ]
