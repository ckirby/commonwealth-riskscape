# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patient_data', '0015_auto_20170328_0654'),
    ]

    operations = [
        migrations.AlterField(
            model_name='riskscapedata',
            name='site',
            field=models.CharField(verbose_name='Practice Group', max_length=3, choices=[('ATR', 'Atrius'), ('CHA', 'CHA'), ('ML', 'MLCHC')], null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='trimtrackerdata',
            name='site',
            field=models.CharField(verbose_name='Practice Group', max_length=3, choices=[('ATR', 'Atrius'), ('CHA', 'CHA'), ('ML', 'MLCHC')], null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='uploaddata',
            name='site_name',
            field=models.CharField(max_length=3, choices=[('ATR', 'Atrius'), ('CHA', 'CHA'), ('ML', 'MLCHC')]),
        ),
    ]
