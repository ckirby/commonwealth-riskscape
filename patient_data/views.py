from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from patient_data.forms import UploadRawDataForm
from patient_data import jobs


@csrf_exempt
def upload_file(request):
    return _process_upload(request, jobs.process_data)


@csrf_exempt
def upload_historic_file(request):
    return _process_upload(request, jobs.process_historic_data)


def _process_upload(request, func):
    if request.method == 'POST':
        form = UploadRawDataForm(request.POST, request.FILES)
        if form.is_valid():
            upload_data = form.save()
            func.delay(upload_data.pk)
            return HttpResponse('Upload Successful')
        else:
            return HttpResponse("Errors %s" % form.errors)
    else:
        return HttpResponse(status=403)
