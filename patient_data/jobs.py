import tarfile
import arrow
from datetime import datetime, date

from django.db import DataError
from django.db import IntegrityError
from django.db import transaction
from django.db.models import Count
from django_rq import job, enqueue

from analysis import view_utils
from analysis.forms import FilterQueryForm
from analysis.forms import OutcomeQueryForm
from analysis.models import SummaryOutcome, Census, DASHBOARD_CONDITIONS, TimeseriesResultCache, MapResultCache, \
    CurrentRateResultCache
from patient_data.RiskscapeCopyMapping import RiskscapeCopyMapping
from patient_data.models import UploadData, TrimtrackerData, RiskscapeData


class LoadError(Exception):
    """Exception raised for errors in the input.

    Attributes:
        site -- site that threw the error
        message -- explanation of the error
    """

    def __init__(self, site, message):
        self.site = site
        self.message = message


@job('upload')
def process_data(upload):
    _load_data(upload, False)


@job('historical')
def process_historic_data(upload):
    _load_data(upload, True)


def _load_data(upload, historic):
    ud = UploadData.objects.get(pk=upload)

    if tarfile.is_tarfile(ud.data_file.path):
        path_parts = ud.data_file.path.split('/')
        path = "{}/{}".format('/'.join(path_parts[:-1]), path_parts[-1:][0].split('.')[0])
        tar = tarfile.open(ud.data_file.path)
        tar_name = tar.getnames()[0]
        tar.extractall(path)
        tar.close()
        ud.data_file.delete(save=True)
        ud.data_file.name = "{}/{}".format(path, tar_name)
        ud.save()

    try:
        ud.status = 1
        ud.save()
        if ud.data_type == 'W':
            process_riskscape(ud)
        elif ud.data_type == 'M':
            process_trimtracker(ud, historic)
        ud.data_file.delete(save=True)
        ud.status = 2
    except (LoadError, ValueError, DataError) as e:
        ud.status = 3
        raise e
    finally:
        ud.processed = datetime.now()
        ud.save()


def process_trimtracker(datafield, historic):
    try:
        with transaction.atomic():
            copymap = RiskscapeCopyMapping(TrimtrackerData, datafield.data_file.path, TrimtrackerData.fieldmap(),
                                           static_mapping={'site': datafield.site_name})
            TrimtrackerData.objects.replace_data(datafield.site_name, copymap, historic)
            copymap.model = RiskscapeData
            copymap.mapping = RiskscapeData.fieldmap()
            RiskscapeData.objects.replace_data(datafield.site_name, copymap)
            MapResultCache.objects.all().update(result=None)
            CurrentRateResultCache.objects.all().update(result=None)
    except IntegrityError as e:
        raise LoadError(datafield.site_name, e)

    _update_outcomes()
    _update_timeseries_cache()
    _update_catchment()


def _update_outcomes():
    SummaryOutcome.objects.exclude(field__in=[condition for condition in DASHBOARD_CONDITIONS]).delete()
    for condition, form_values in DASHBOARD_CONDITIONS.items():
        _update_outcome.delay(condition, form_values)


@job
def _update_outcome(condition, form_values):
    today = date.today()
    outcome_form = OutcomeQueryForm(form_values['outcome'])
    filter_form = FilterQueryForm(form_values['filter'])
    if outcome_form.is_valid() and filter_form.is_valid():
        pass
    else:
        print("{} error".format(condition))

    try:
        so = SummaryOutcome.current.get(field=condition,
                                        date__month=today.month,
                                        date__year=today.year)
    except SummaryOutcome.DoesNotExist:
        so = SummaryOutcome(field=condition)
    so.save()


def _update_timeseries_cache():
    TimeseriesResultCache.objects.all().update(result=None)
    dashboard_conditions = _bootstrap_outcome_cache()
    for dc in dashboard_conditions:
        _update_single_timeseries_cache.delay(dc)

    result_cache = TimeseriesResultCache.objects.filter(result=None, viewed__gt=1,
                                                        last_accessed__gt=arrow.now().shift(days=-45).date())\
                                                .values_list('pk', flat=True)
    for cache in set(result_cache) - set(dashboard_conditions):
        _update_single_timeseries_cache.delay(cache)


@job
def _update_single_timeseries_cache(pk):
    trc = TimeseriesResultCache.objects.get(pk=pk)
    outcome_form = OutcomeQueryForm(trc.outcome)
    filter_form = FilterQueryForm(trc.filter)
    if outcome_form.is_valid() and filter_form.is_valid():
        view_utils.timeseries([{"location": trc.location}], outcome_form, filter_form, trc.stratify, trc.time_chunk,
                              force_cache_update=True)
    else:
        trc.delete()


@transaction.atomic
def _bootstrap_outcome_cache(condition_name=None):
    dcl = []
    for condition, form_values in DASHBOARD_CONDITIONS.items():
        if condition_name is not None and not condition == condition_name:
            continue
        outcome_form = OutcomeQueryForm(form_values['outcome'])
        filter_form = FilterQueryForm(form_values['filter'])
        if outcome_form.is_valid() and filter_form.is_valid():
            for stratify in ['none', 'age_group', 'sex', 'race_ethnicity']:
                trc = TimeseriesResultCache.objects.cache_results(outcome_form=outcome_form,
                                                                  filter_form=filter_form,
                                                                  results=None,
                                                                  location='Massachusetts',
                                                                  stratify=stratify,
                                                                  time_chunk='month')
                dcl.append(trc.pk)
    return dcl


def _get_ratio(counts, idx):
    if counts[idx]['den'] == 0:
        return 0
    return float(counts[idx]['num'] * 100) / float(counts[idx]['den'])


@transaction.atomic
def process_riskscape(datafield):
    try:
        with transaction.atomic():
            RiskscapeData.objects.replace_data(datafield.site_name, datafield.data_file.path)
            MapResultCache.objects.all().update(result=None)
            CurrentRateResultCache.objects.all().update(result=None)
    except IntegrityError as e:
        raise LoadError(datafield.site_name, e)

    _update_catchment()


@transaction.atomic
def _update_catchment():
    for loc in RiskscapeData.objects.filter(encounters_two_year__gt=0).values('census_id').order_by(
            'census_id').annotate(total=Count('census_id')).values('census_id', 'total'):
        try:
            cen = Census.objects.get(zip=loc['census_id'])
            cen.catchment = loc['total']
            cen.save()
        except Census.DoesNotExist:
            pass


def _update_single_timeseries_cache_exception_handler(failed_job, *exc_info):
    if failed_job.func == _update_single_timeseries_cache and failed_job.timeout < 1600:
        enqueue(_update_single_timeseries_cache, args=failed_job.args, timeout=failed_job.timeout + 100)
        return False
    return True
