import os
from datetime import date

import arrow
from django.db import models
from django.db import transaction
from django.db.models import Case, IntegerField, Q, When, Sum

from patient_data import model_choices


def path_and_rename(instance, filename):
    upload_to = 'upload'
    filename = '{}_{}_{}'.format(date.today().strftime('%Y-%m-%d'), instance.site_name, filename)
    return os.path.join(upload_to, filename)


class UploadDataManager(models.Manager):
    def most_recent_status_by_site(self):
        statuses = {}
        for site in model_choices.SITE_CHOICES:
            try:
                statuses[site[1]] = self.filter(site_name=site[0]).latest('id').get_status_display()
            except UploadData.DoesNotExist:
                pass
        return statuses


class UploadData(models.Model):
    data_file = models.FileField(max_length=255, upload_to=path_and_rename)
    site_name = models.CharField(max_length=3, choices=model_choices.SITE_CHOICES, blank=False, null=False)
    data_type = models.CharField(max_length=1, choices=(('W', 'Weekly'), ('M', 'Monthly')), blank=False,
                                 null=False)
    processed = models.DateTimeField(null=True)
    status = models.IntegerField(choices=((0, "Pending"), (1, "Running"), (2, "Success"), (3, "Failure")), default=0)

    objects = UploadDataManager()

    def __str__(self):
        return "{}-{} data {}".format(self.data_type, self.get_site_name_display(), self.get_status_display())


class PatientDataManager(models.Manager):
    @transaction.atomic
    def replace_data(self, site, copymap, full_history=False):
        self.clear_data(site, full_history)
        self.add_data(copymap)
        self.remove_too_old(site)
        self.remove_uninteresting_zips(site)

    def clear_data(self, site, full_history=False):
        delete_filter = {'site': site}
        if not full_history and self.model == TrimtrackerData:
            delete_filter['year__gt'] = arrow.now().year - 2
        self.filter(**delete_filter).delete()

    def add_data(self, copymap):
        copymap.save()

    def remove_uninteresting_zips(self, site):
        self.filter(census__isnull=True, site=site).delete()

    def remove_too_old(self, site):
        if self.model == TrimtrackerData:
            self.filter(site=site, year__lt=2012).delete()

    def qs_ratio(self, qs=None, outcome_filters=None, outcome_dict=None):
        if qs is None:
            qs = self.model.all()

        numerator_q = {}
        if outcome_filters is None and outcome_dict is None:
            return qs
        elif outcome_filters:
            for key, val in outcome_filters.cleaned_data.items():
                if key in outcome_filters.changed_data:
                    numerator_q['{}__in'.format(key)] = val
        elif outcome_dict:
            for key, val in outcome_dict.items():
                numerator_q['{}__in'.format(key)] = val

        qs = qs.annotate(
            denominator=Sum(Case(When(Q(pk__isnull=False), then=1), default=0, output_field=IntegerField())),
            numerator=Sum(Case(When(Q(**numerator_q), then=1), default=0, output_field=IntegerField()))
        )

        return qs


class PatientData(models.Model):
    site = models.CharField('Practice Group',
                            max_length=3,
                            choices=model_choices.SITE_CHOICES,
                            null=True,
                            blank=True)
    age_group = models.PositiveSmallIntegerField('Age Group',
                                                 choices=model_choices.AGE_CHOICES, null=True)
    sex = models.PositiveSmallIntegerField('Sex',
                                           choices=model_choices.SEX_CHOICES, null=True, )
    race_ethnicity = models.PositiveSmallIntegerField('Race/Ethnicity',
                                                      choices=model_choices.RACE_ETHNICITY_CHOICES, null=True)
    birth_cohort = models.PositiveSmallIntegerField('Birth Cohort',
                                                    choices=model_choices.BIRTH_COHORT_CHOICES, null=True)
    census = models.ForeignKey('analysis.Census', null=True)
    encounters_one_year = models.PositiveSmallIntegerField('Encounters in Last Year',
                                                           blank=True,
                                                           null=True,
                                                           default=0)
    encounters_two_year = models.PositiveSmallIntegerField('Encounters in Last Two Years',
                                                           blank=True,
                                                           null=True,
                                                           default=0)
    encounters_total = models.PositiveSmallIntegerField('Lifetime Encounters',
                                                        blank=True,
                                                        null=True,
                                                        default=0)
    bmi = models.PositiveSmallIntegerField('BMI',
                                           choices=model_choices.BMI_CHOICES,
                                           blank=True,
                                           null=True)
    bmi_percent = models.PositiveSmallIntegerField('BMI Percentile',
                                                   choices=model_choices.BMI_PERCENT_CHOICES,
                                                   blank=True,
                                                   null=True)
    pregnant = models.PositiveSmallIntegerField('Pregnant',
                                                choices=model_choices.BOOL_CHOICES,
                                                blank=True,
                                                null=True)
    gdm = models.PositiveSmallIntegerField('GDM',
                                           choices=model_choices.BOOL_CHOICES,
                                           blank=True,
                                           null=True)
    sysbp = models.PositiveSmallIntegerField('Systolic Blood Pressure',
                                             choices=model_choices.BP_CHOICES,
                                             blank=True,
                                             null=True)
    sbp_percent = models.IntegerField('SBP Percentile',
                                      choices=model_choices.BMI_PERCENT_CHOICES,
                                      blank=True,
                                      null=True)
    diabp = models.PositiveSmallIntegerField('Diastolic Blood Pressure',
                                             choices=model_choices.BP_CHOICES,
                                             blank=True,
                                             null=True)
    dbp_percent = models.PositiveSmallIntegerField('DBP Percentile',
                                                   choices=model_choices.BMI_PERCENT_CHOICES,
                                                   blank=True,
                                                   null=True)
    ldl = models.PositiveSmallIntegerField('LDL Cholesterol',
                                           choices=model_choices.LDL_CHOICES,
                                           blank=True,
                                           null=True)
    tg = models.PositiveSmallIntegerField('Triglycerides',
                                          choices=model_choices.TG_CHOICES,
                                          blank=True,
                                          null=True)
    a1c = models.PositiveSmallIntegerField('A1C',
                                           choices=model_choices.A1C_CHOICES,
                                           blank=True,
                                           null=True)
    pre_dm = models.PositiveSmallIntegerField('Pre-Diabetes',
                                              choices=model_choices.BOOL_CHOICES,
                                              blank=True,
                                              null=True)
    dm1 = models.PositiveSmallIntegerField('Type 1 Diabetes',
                                           choices=model_choices.BOOL_CHOICES,
                                           blank=True,
                                           null=True)
    dm2 = models.PositiveSmallIntegerField('Type 2 Diabetes',
                                           choices=model_choices.BOOL_CHOICES,
                                           blank=True,
                                           null=True)
    insulin = models.PositiveSmallIntegerField('Insulin Prescription',
                                               choices=model_choices.BOOL_CHOICES,
                                               blank=True,
                                               null=True)
    metformin = models.PositiveSmallIntegerField('Metformin',
                                                 choices=model_choices.BOOL_CHOICES,
                                                 blank=True,
                                                 null=True)
    curr_flu_vac = models.PositiveSmallIntegerField('Flu Vaccine',
                                                    choices=model_choices.BOOL_CHOICES,
                                                    blank=True,
                                                    null=True)
    smoker = models.PositiveSmallIntegerField('Smoking Status',
                                              choices=model_choices.SMOKING_CHOICES,
                                              blank=True,
                                              null=True)
    asthma = models.PositiveSmallIntegerField('Asthma',
                                              choices=model_choices.BOOL_CHOICES,
                                              blank=True,
                                              null=True)
    chlamydia = models.PositiveSmallIntegerField('Chlamydia',
                                                 choices=model_choices.POS_NEG_IND_CHOICES,
                                                 blank=True,
                                                 null=True)
    depression = models.PositiveSmallIntegerField('Depression (Rx)',
                                                  choices=model_choices.BOOL_CHOICES,
                                                  blank=True,
                                                  null=True)
    opioid_rx = models.PositiveSmallIntegerField('Opioid Rx',
                                                 choices=model_choices.BOOL_CHOICES,
                                                 blank=True,
                                                 null=True)
    benzo_rx = models.PositiveSmallIntegerField('Benzo Rx',
                                                choices=model_choices.BOOL_CHOICES,
                                                blank=True,
                                                null=True)
    opioid_benzo_concurrent = models.PositiveSmallIntegerField('Benzo and Opioid',
                                                               choices=model_choices.BOOL_CHOICES,
                                                               blank=True,
                                                               null=True)
    high_opiod_rx = models.PositiveSmallIntegerField('High Dose Opioid',
                                                     choices=model_choices.BOOL_CHOICES,
                                                     blank=True,
                                                     null=True)
    gonorrhea = models.PositiveSmallIntegerField('Gonorrhea',
                                                 choices=model_choices.POS_NEG_IND_CHOICES,
                                                 blank=True,
                                                 null=True)
    hypertension = models.PositiveSmallIntegerField('Hypertension',
                                                    choices=model_choices.HYPERTENSION_CHOICES,
                                                    blank=True,
                                                    null=True)
    ili_current = models.PositiveSmallIntegerField('Seasonal ILI (monthly)',
                                                   choices=model_choices.BOOL_CHOICES,
                                                   blank=True,
                                                   null=True)
    ili_cum = models.PositiveSmallIntegerField('Seasonal ILI (cumulative)',
                                               choices=model_choices.BOOL_CHOICES,
                                               blank=True,
                                               null=True)
    hepc_test = models.PositiveSmallIntegerField('HCV Elisa or RNA Test',
                                                 choices=model_choices.BOOL_CHOICES,
                                                 blank=True,
                                                 null=True)
    lyme = models.PositiveSmallIntegerField('Lyme monthly',
                                            choices=model_choices.BOOL_CHOICES,
                                            blank=True,
                                            null=True)
    lyme_last = models.PositiveSmallIntegerField('Lyme cumulative (last year)',
                                                 choices=model_choices.BOOL_CHOICES,
                                                 blank=True,
                                                 null=True)
    syph_test = models.PositiveSmallIntegerField('Syphilis Screen',
                                                 choices=model_choices.SYPH_CHOICES,
                                                 blank=True,
                                                 null=True)
    tdap = models.PositiveSmallIntegerField('TDaP Vaccine',
                                            choices=model_choices.BOOL_CHOICES,
                                            blank=True,
                                            null=True)
    tdap_preg = models.PositiveSmallIntegerField('TDaP Vaccine in Pregnancy',
                                                 choices=model_choices.BOOL_CHOICES,
                                                 blank=True,
                                                 null=True)
    lyme_cum = models.PositiveSmallIntegerField('Lyme cumulative (this year)',
                                                choices=model_choices.BOOL_CHOICES,
                                                blank=True,
                                                null=True)
    pertussis = models.PositiveSmallIntegerField('Pertussis syndrome (monthly)',
                                                 choices=model_choices.BOOL_CHOICES,
                                                 blank=True,
                                                 null=True)
    pertussis_last = models.PositiveSmallIntegerField('Pertussis syndrome cumulative last 12 months',
                                                      choices=model_choices.BOOL_CHOICES,
                                                      blank=True,
                                                      null=True)

    class Meta:
        abstract = True

    def copy_census_template(self):
        from analysis.models import Census
        return """
          CASE
              WHEN "%(name)s" in ('{}') THEN "%(name)s"
              ELSE null
          END
          """.format("','".join([str(x) for x in Census.objects.values_list('zip', flat=True)]))

    copy_census_template.copy_type = 'text'

    @classmethod
    def fieldmap(cls):
        return {
            'sex': 'ab',
            'race_ethnicity': 'ac',
            'birth_cohort': 'ad',
            'census': 'ae',
            'age_group': 'ah',
            'encounters_one_year': 'ai',
            'encounters_two_year': 'aj',
            'bmi': 'ak',
            'bmi_percent': 'al',
            'pregnant': 'am',
            'gdm': 'an',
            'sysbp': 'ao',
            'sbp_percent': 'ap',
            'diabp': 'aq',
            'dbp_percent': 'ar',
            'ldl': 'as',
            'tg': 'at',
            'a1c': 'au',
            'pre_dm': 'av',
            'dm1': 'aw',
            'dm2': 'ax',
            'insulin': 'ay',
            'metformin': 'az',
            'curr_flu_vac': 'ba',
            'smoker': 'bb',
            'asthma': 'bc',
            'chlamydia': 'bd',
            'depression': 'be',
            'opioid_rx': 'bf',
            'benzo_rx': 'bg',
            'opioid_benzo_concurrent': 'bh',
            'high_opiod_rx': 'bi',
            'gonorrhea': 'bj',
            'hypertension': 'bk',
            'ili_current': 'bl',
            'hepc_test': 'bm',
            'lyme': 'bn',
            'syph_test': 'bo',
            'tdap': 'bp',
            'tdap_preg': 'bq',
            'lyme_last': 'br',
            'encounters_total': 'bs',
            'ili_cum': 'bt',
            'lyme_cum': 'bu',
            'pertussis': 'bv',
            'pertussis_last': 'bw'
        }


class RiskscapeData(PatientData):
    objects = PatientDataManager()


class TrimtrackerData(PatientData):
    month = models.IntegerField(choices=model_choices.MONTHS.items())
    year = models.IntegerField(db_index=True)
    quarter = models.IntegerField(null=True)

    objects = PatientDataManager()

    def copy_month_template(self):
        return """
          CASE
              WHEN "%(name)s" = '01' THEN 1
              WHEN "%(name)s" = '02' THEN 2
              WHEN "%(name)s" = '03' THEN 3
              WHEN "%(name)s" = '04' THEN 4
              WHEN "%(name)s" = '05' THEN 5
              WHEN "%(name)s" = '06' THEN 6
              WHEN "%(name)s" = '07' THEN 7
              WHEN "%(name)s" = '08' THEN 8
              WHEN "%(name)s" = '09' THEN 9
              WHEN "%(name)s" = '10' THEN 10
              WHEN "%(name)s" = '11' THEN 11
              WHEN "%(name)s" = '12' THEN 12
          END
          """

    copy_month_template.copy_type = 'text'

    def copy_quarter_template(self):
        return """
          CASE
              WHEN "%(name)s" = '01' THEN 1
              WHEN "%(name)s" = '02' THEN 1
              WHEN "%(name)s" = '03' THEN 1
              WHEN "%(name)s" = '04' THEN 2
              WHEN "%(name)s" = '05' THEN 2
              WHEN "%(name)s" = '06' THEN 2
              WHEN "%(name)s" = '07' THEN 3
              WHEN "%(name)s" = '08' THEN 3
              WHEN "%(name)s" = '09' THEN 3
              WHEN "%(name)s" = '10' THEN 4
              WHEN "%(name)s" = '11' THEN 4
              WHEN "%(name)s" = '12' THEN 4
          END
          """

    copy_quarter_template.copy_type = 'text'

    class Meta:
        ordering = ['-year', '-month']
        index_together = (('year', 'month'), ('year', 'quarter'))

    @classmethod
    def fieldmap(cls):
        field_map = super(TrimtrackerData, cls).fieldmap()
        field_map['year'] = 'af'
        field_map['month'] = 'ag'
        field_map['quarter'] = 'ag'
        return field_map
