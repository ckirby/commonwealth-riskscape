"""
WSGI config for trimscape project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""
from django.core.handlers.wsgi import WSGIHandler
import django
import os


class WSGIEnvironment(WSGIHandler):

    def __call__(self, environ, start_response):
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "trimscape.settings")
        django.setup()
        return super(WSGIEnvironment, self).__call__(environ, start_response)

application = WSGIEnvironment()
