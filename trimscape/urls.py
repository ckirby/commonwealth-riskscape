from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from registration.views import ActivationCompleteView
from registration.views import RegistrationClosedView
from registration.views import RegistrationCompleteView
from registration.views import RegistrationView

from riskscape_registration.views import RiskscapeActivationView
from trimscape import settings

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'trimscape/login.html'},
        name='login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/accounts/login'}),
    url(r'^data/$', 'patient_data.views.upload_file'),
    url(r'^data/historic/$', 'patient_data.views.upload_historic_file'),
    url(r'^$', 'analysis.views.home', name='home'),
    url(r'^(?P<outcome>[\w ]+)$', 'analysis.views.heat_map', name='outcome'),
    url(r'^map/$', 'analysis.views.heat_map', name='map'),
    url(r'^map/(?P<pk>\d+)/$', 'analysis.views.map_overlay', name='map_overlay'),
    url(r'^current/$', 'analysis.views.graph_view', {"graph_type": "current"}, name='current'),
    url(r'^current/(?P<outcome>[\w ]+)$', 'analysis.views.graph_view', {"graph_type": "current"},
        name='current-outcome'),
    url(r'^current/(?P<pk1>\d+)/(?P<pk2>-?\d+)/$', 'analysis.views.current_graph', name='current_cache'),
    url(r'^timeseries/$', 'analysis.views.graph_view', {"graph_type": "timeseries"}, name='timeseries'),
    url(r'^timeseries/(?P<outcome>[\w ]+)$', 'analysis.views.graph_view', {"graph_type": "timeseries"},
        name='timeseries-outcome'),
    url(r'^timeseries/(?P<pk1>\d+)/(?P<pk2>-?\d+)/$', 'analysis.views.timeseries_graph', name='timeseries_cache'),
    url(r'^trendline/(?P<inflection>\d+)/timeseries/(?P<pk1>\d+)/(?P<pk2>-?-?\d+)/$',
        'analysis.views.timeseries_trend', name='timeseries_trendline'),
    url(r'^about/$', 'analysis.views.about', name='about'),
    url(r'^coc/$', 'coc.views.coc_home', name='coc_home'),
    url(r'^coc/(?P<slug>[\w ]+)$', 'coc.views.coc', name='coc'),
    url(r'^coc/upload/$', 'coc.views.process_upload', name='coc-upload'),
    url(r'^django-rq/', include('django_rq.urls')),
    url(r'^registration/activate/complete/$', ActivationCompleteView.as_view(),
        name='registration_activation_complete'),
    url(r'^registration/activate/(?P<activation_key>\w+)/$',
        RiskscapeActivationView.as_view(template_name='riskscape_registration/activation_form.html'),
        name='registration_activate'),
    url(r'^registration/register/$',
        RegistrationView.as_view(template_name='riskscape_registration/registration_form.html'),
        name='registration_register'),
    url(r'^registration/register/closed/$', RegistrationClosedView.as_view(),
        name='registration_disallowed'),
    url(r'^registration/register/complete/$',
        RegistrationCompleteView.as_view(template_name='riskscape_registration/registration_complete.html'),
        name='registration_complete'),
    url(r'^registration/review/$', 'riskscape_registration.views.review', name='registration-review'),
    url(r'^registration/review/approve/(?P<pk>\d+)/$', 'riskscape_registration.views.approve', name='registration-approve'),
    url(r'^registration/review/deny/(?P<pk>\d+)/$', 'riskscape_registration.views.deny', name='registration-deny'),
    url(r'^registration/review/retry/(?P<pk>\d+)/$', 'riskscape_registration.views.retry', name='registration-retry'),
    url(r'^registration/review/delete/(?P<pk>\d+)/$', 'riskscape_registration.views.delete', name='registration-delete'),
]

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
