from django.contrib import admin

# Register your models here.
from analysis.models import SummaryOutcome, Census

admin.site.register(SummaryOutcome)
admin.site.register(Census)
