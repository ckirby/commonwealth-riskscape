import json
from datetime import date

import arrow
import jsonfield
from django.db import models
from django.db.models import F, Sum

from analysis.forms import OutcomeQueryForm, FilterQueryForm
from patient_data.models import RiskscapeData

ENCOUNTER_FILTER = {'encounters_one_year': 'Historic', 'encounters_total': 1}
DASHBOARD_CONDITIONS = {
    "Type 2 Diabetes":
        {"outcome": {'dm2': [1, ]},
         "filter": dict(ENCOUNTER_FILTER, **{'age_group': [5, 6, 7, 8, 9, 10, 11, 12]})
         },
    "Smoking":
        {"outcome": {'smoker': [4, ]},
         "filter": dict(ENCOUNTER_FILTER, **{'age_group': [5, 6, 7, 8, 9, 10, 11, 12]})
         },
    "Hypertension":
        {"outcome": {'hypertension': [1, ]},
         "filter": dict(ENCOUNTER_FILTER, **{'age_group': [5, 6, 7, 8, 9, 10, 11, 12]})
         },
    "Pediatric Asthma":
        {"outcome": {'asthma': [1, ]},
         "filter": dict(ENCOUNTER_FILTER, **{'age_group': [1, 2, 3, 4]})
         },
    "Obesity":
        {"name": "Obesity (BMI &gt;30)",
         "outcome": {'bmi': [3, ]},
         "filter": dict(ENCOUNTER_FILTER, **{'age_group': [5, 6, 7, 8, 9, 10, 11, 12]})
         },
    "Overweight":
        {"name": "Overweight (BMI 25-30)",
         "outcome": {'bmi': [2, ]},
         "filter": dict(ENCOUNTER_FILTER, **{'age_group': [5, 6, 7, 8, 9, 10, 11, 12]})
         },
    "Influenza like illness Monthly":
        {"name": "Influenza-like illness (Monthly)",
         "outcome": {'ili_current': [1, ]},
         "filter": dict(ENCOUNTER_FILTER),
         "chart": "S"
         },
    "Influenza like illness Cumulative":
        {"name": "Influenza-like illness (Cumulative)",
         "outcome": {'ili_cum': [1, ]},
         "filter": dict(ENCOUNTER_FILTER),
         "chart": "S"
         },
    "Curr ILI vac":
        {"name": "Influenza vaccination, current season",
         "outcome": {'curr_flu_vac': [1, ]},
         "filter": dict(ENCOUNTER_FILTER)
         },
    "Lyme disease Monthly":
        {"name": "Lyme disease (Monthly)",
         "outcome": {'lyme': [1, ]},
         "filter": dict(ENCOUNTER_FILTER),
         "chart": "S"
         },
    "Lyme disease Cumulative":
        {"name": "Lyme disease (Cumulative)",
         "outcome": {'lyme_cum': [1, ]},
         "filter": dict(ENCOUNTER_FILTER),
         "chart": "S"
         },
    "Pertussis syndrome Monthly":
        {"name": "Pertussis syndrome (Monthly)",
         "outcome": {'pertussis': [1, ]},
         "filter": dict(ENCOUNTER_FILTER),
         "chart": "S"
         },
    "Pertussis syndrome Cumulative":
        {"name": "Pertussis syndrome (Cumulative)",
         "outcome": {'pertussis_last': [1, ]},
         "filter": dict(ENCOUNTER_FILTER),
         "chart": "S"
         },
    "Syphilis Test Pregnant":
        {"name": "Syphilis screening in pregnant women",
         "outcome": {'syph_test': [1, ]},
         "filter": dict(ENCOUNTER_FILTER, **{"sex": [2, ], 'pregnant': [1, ]})
         },
    "Hepc Boomer":
        {"name": "Hepatitis C screening in patients born 1945-1965",
         "outcome": {'hepc_test': [1, ]},
         "filter": dict(ENCOUNTER_FILTER, **{"birth_cohort": [1, ]})
         },
    "Young Female Chlamy":
        {"name": "Chlamydia screening in women aged 15-24",
         "outcome": {'chlamydia': [1, 2, 3, ]},
         "filter": dict(ENCOUNTER_FILTER, **{"sex": [2, ], "age_group": [4, 5]})
         },
    "Depression":
        {"name": "Depression (Rx)",
         "outcome": {'depression': [1, ]},
         "filter": dict(ENCOUNTER_FILTER, **{'age_group': [5, 6, 7, 8, 9, 10, 11, 12]})
         },
    "Opioid Rx":
        {"name": "Opioid Prescription",
         "outcome": {'opioid_rx': [1, ]},
         "filter": dict(ENCOUNTER_FILTER, **{'age_group': [5, 6, 7, 8, 9, 10, 11, 12]})
         },
}


class CurrentSummaryOutcomeManager(models.Manager):
    def get_queryset(self):
        try:
            latest_date = SummaryOutcome.objects.latest('date').date
        except SummaryOutcome.DoesNotExist:
            latest_date = date.today()
        return super(CurrentSummaryOutcomeManager, self).get_queryset() \
            .filter(date=latest_date).order_by('field')


class SummaryOutcome(models.Model):
    date = models.DateField(auto_now=True)
    field = models.CharField(max_length=50)
    chart_type = models.CharField(max_length=1, choices=(('D', 'Donut'), ('S', 'Sparkline')))

    objects = models.Manager()
    current = CurrentSummaryOutcomeManager()

    class Meta:
        ordering = ['-date']
        unique_together = ('field', 'date')

    def __str__(self):
        return '{} for {}-{}'.format(self.display_name, self.date.year, self.date.month)

    def save(self, *args, **kwargs):
        self.chart_type = DASHBOARD_CONDITIONS[self.field].get("chart", "D")
        super(SummaryOutcome, self).save(*args, **kwargs)

    @property
    def _get_forms(self):
        form_values = DASHBOARD_CONDITIONS[self.field]
        outcome_form = OutcomeQueryForm(form_values['outcome'])
        filter_form = FilterQueryForm(form_values['filter'])
        if outcome_form.is_valid() and filter_form.is_valid():
            return outcome_form, filter_form
        return None

    def fetch_value(self, offset):
        outcome_form, filter_form = self._get_forms
        trc = TimeseriesResultCache.objects.get_cached_results(outcome_form, filter_form,
                                                               location="Massachusetts",
                                                               time_chunk="month",
                                                               stratify="none")
        if trc is None or not trc.result:
            return None

        result = trc.result
        offset = -offset - 1

        try:
            result = result['All'][offset:][0]
            return (result['numerator'] / result['denominator']) * 100
        except (AttributeError, KeyError, ZeroDivisionError):
            return None

    @property
    def trendline(self):
        if self.chart_type == "D":
            return None

        outcome_form, filter_form = self._get_forms
        trc = TimeseriesResultCache.objects.get_cached_results(outcome_form, filter_form,
                                                               location="Massachusetts",
                                                               time_chunk="month",
                                                               stratify="none")
        if trc is None or not trc.result:
            return ''

        result = trc.result
        ratio = []
        trend_lookback = arrow.get(self.date.year, self.date.month, 15).shift(months=-37)
        for t in result['All']:
            if self._is_current(t, trend_lookback):
                if t['denominator'] == -1:
                    ratio.append(-1)
                else:
                    ratio.append(t['numerator'] / t['denominator'])

        return ratio

    def _is_current(self, t, lookback):
        if self.chart_type == "D":
            return False

        if arrow.get(t['year'], t['month'], 15) >= lookback:
            return True

        return False

    @property
    def display_name(self):
        return DASHBOARD_CONDITIONS[self.field].get("name", self.field)

    @property
    def values_of_interest(self):
        ff = FilterQueryForm(DASHBOARD_CONDITIONS[self.field]['filter'])
        if ff.is_valid():
            pass
        filter_strings = []
        for key, value in ff.pretty_print_query().items():
            filter_strings.append("{}: {}".format(key, value))

        return "<br/>".join(filter_strings)

    def _display_value(self, value):
        if value is None:
            return ''
        return format(value, '.2f')

    @property
    def this_month(self):
        return self._display_value(self.fetch_value(0))

    @property
    def most_recent(self):
        return "{}%".format(self._display_value(self.fetch_value(6)))

    @property
    def next_most_recent(self):
        return "{}%".format(self._display_value(self.fetch_value(12)))

    @property
    def least_recent(self):
        return "{}%".format(self._display_value(self.fetch_value(24)))


class CensusManager(models.Manager):
    def get_caught_pop(self):
        return self.aggregate(caught=Sum('catchment'))['caught']


class Census(models.Model):
    zip = models.CharField(max_length='5', primary_key=True)
    population = models.PositiveIntegerField(blank=True, null=True)
    catchment = models.PositiveIntegerField(blank=True, null=True)
    coverage = models.CharField(max_length=7, default="Unknown")

    objects = CensusManager()

    class Meta:
        ordering = ['zip']

    def __str__(self):
        return '{}'.format(self.zip)

    def save(self, *args, **kwargs):
        if self.population and self.catchment:
            self.coverage = format(self.catchment * 100 / self.population, '.1f')
        super(Census, self).save(*args, **kwargs)


class City(models.Model):
    city = models.CharField(max_length=32, primary_key=True)
    zips = models.ManyToManyField(Census)

    class Meta:
        ordering = ['city']

    def __str__(self):
        return self.city


class ResultCacheManager(models.Manager):
    def cache_results(self, outcome_form, filter_form, results, **kwargs):
        kwargs["outcome_form"] = json.dumps(outcome_form.cleaned_data, sort_keys=True)
        kwargs["filter_form"] = json.dumps(filter_form.cleaned_data, sort_keys=True)
        rc, created = self.get_or_create(**kwargs)
        if results is not None:
            rc.result = results
            rc.last_accessed = date.today()
            rc.save()

        return rc

    def _get_cache(self, outcome_form, filter_form, **kwargs):
        kwargs["outcome_form"] = json.dumps(outcome_form.cleaned_data, sort_keys=True)
        kwargs["filter_form"] = json.dumps(filter_form.cleaned_data, sort_keys=True)
        rc = self.get(**kwargs)
        if rc.result is not None:
            rc.viewed = F('viewed') + 1
            rc.last_accessed = date.today()
            rc.save()

        return rc

    def get_cached_results(self, outcome_form, filter_form, **kwargs):
        try:
            trc = self._get_cache(outcome_form, filter_form, **kwargs)
            return trc
        except self.model.DoesNotExist:
            return None
        except self.model.MultipleObjectsReturned:
            return None


class ResultCache(models.Model):
    outcome_form = jsonfield.JSONField()
    filter_form = jsonfield.JSONField()
    result = jsonfield.JSONField(null=True, blank=True)
    viewed = models.IntegerField(default=1)
    last_accessed = models.DateField(auto_created=True, null=True, blank=True)

    objects = ResultCacheManager()

    class Meta:
        abstract = True

    @property
    def outcome(self):
        return json.loads(self.outcome_form)

    @property
    def filter(self):
        return json.loads(self.filter_form)


class CurrentRateResultCache(ResultCache):
    location = models.CharField(max_length=32)
    stratify = models.CharField(max_length=16, null=True, blank=True)

    class Meta:
        unique_together = ('outcome_form', 'filter_form', 'location', 'stratify')


class TimeseriesResultCache(ResultCache):
    location = models.CharField(max_length=32)
    time_chunk = models.CharField(max_length=10)
    stratify = models.CharField(max_length=16, null=True, blank=True)

    class Meta:
        unique_together = ('outcome_form', 'filter_form', 'location', 'stratify', 'time_chunk')


class MapResultCache(ResultCache):
    class Meta:
        unique_together = ('outcome_form', 'filter_form')
