import arrow

from analysis.models import SummaryOutcome, Census, City
from analysis.view_utils import get_quarter, get_data_time_range
from patient_data.models import UploadData
from trimscape import utils


def current_outcomes(request):
    values = SummaryOutcome.current.all()
    outcomes = {}
    try:
        for v in values:
            try:
                outcome = {'name': v.field,
                           'display_name': v.display_name,
                           'interest': v.values_of_interest,
                           'value': v.this_month,
                           'trendline': v.trendline,
                           'chart_type': v.chart_type
                           }
                outcomes[v.field] = outcome
            except TypeError:
                pass

        donuts = [outcomes.get("Type 2 Diabetes", {}),
                  outcomes.get("Smoking", {}),
                  outcomes.get("Hypertension", {}),
                  outcomes.get("Pediatric Asthma", {}),
                  outcomes.get("Obesity", {}),
                  outcomes.get("Overweight", {}),
                  outcomes.get("Curr ILI vac", {}),
                  outcomes.get("Depression", None),
                  outcomes.get("Opioid Rx", None),
                  outcomes.get("Syphilis Test Pregnant", {}),
                  outcomes.get("Hepc Boomer", {}),
                  outcomes.get("Young Female Chlamy", {})]

        donuts = utils.pad_dashboard(donuts)

        sparks = [outcomes.get("Influenza like illness Monthly", {}),
                  outcomes.get("Lyme disease Monthly", {}),
                  outcomes.get("Pertussis syndrome Monthly", {}),
                  outcomes.get("Influenza like illness Cumulative", {}),
                  outcomes.get("Lyme disease Cumulative", {}),
                  outcomes.get("Pertussis syndrome Cumulative", {})]
        sparks = utils.pad_dashboard(sparks)

        cards = donuts + sparks
        if cards[-3:] == [None, None, None]:
            cards = cards[:-3]
        return {"outcomes": utils.nest(cards)}
    except (AttributeError, TypeError):
        return {}


def last_outcome_update(request):
    lou = {"last_outcome_update": None}
    try:
        lou["last_outcome_update"] = SummaryOutcome.current.first().date
    except AttributeError:
        pass

    lou['total_caught'] = Census.objects.get_caught_pop()

    if request.user.is_staff:
        for site, status in UploadData.objects.most_recent_status_by_site().items():
            lou[status] = lou.get(status, [])
            lou[status].append(site)

    return lou


def ranged_stratifiers(request):
    return {"ranged_stratifiers": ["age_group", "tg", "ldl", "bmi_percent", "sbp_percent", "dbp_percent", "a1c"]}


def graphable_selectors(request):
    static_stratifications = [
        {"name": "none", "label": "Unstratified"},
        {"name": "age_group", "label": "Age"},
        {"name": "sex", "label": "Sex"},
        {"name": "race_ethnicity", "label": "Race"},
        {"name": "birth_cohort", "label": "Calendar year at birth"},
        {"name": "site", "label": "Practice Group"}
    ]
    location_list = {"zips": Census.objects.values_list('zip', flat=True),
                     "cities": City.objects.values_list('city', flat=True)}

    return {"static_stratifications": static_stratifications, "location_list": location_list}


def date_range(request):
    try:
        drm, drq, dry, start, end = get_data_time_range()

        months = [{'display': m.format("MMM `YY"), 'quarter': get_quarter(m.month), 'month': m.month, 'year': m.year}
                  for
                  m in drm]
        quarters = [{'display': "{} `{}".format(get_quarter(q.month), q.format('YY')), 'quarter': get_quarter(q.month),
                     'month': q.month, 'year': q.year} for q in drq]
        years = [{'display': y.format('YYYY'), 'quarter': 'Q1', 'month': 1, 'year': y.year} for y in dry]

        context = {'month_range': months, 'quarter_range': quarters, 'year_range': years}
        context['default_start'] = {'quarter': get_quarter(start.month), 'month': start.month, 'year': start.year}
        context['default_end'] = {'quarter': get_quarter(end.month), 'month': end.month, 'year': end.year}

        return context
    except:
        return {}

