import math

import arrow
from django.http import JsonResponse
from django.shortcuts import get_object_or_404

from analysis.models import City, TimeseriesResultCache, CurrentRateResultCache
from analysis.stats import Stats
from patient_data.models import TrimtrackerData


def current_rates(data, outcome_form, filter_form, stratify):
    from analysis import jobs
    for datum in data:
        crc = CurrentRateResultCache.objects.get_cached_results(outcome_form, filter_form, location=datum['location'],
                                                                stratify=stratify)
        if crc is None or crc.result is None:
            crc = CurrentRateResultCache.objects.cache_results(outcome_form, filter_form, None,
                                                               location=datum['location'], stratify=stratify)
            jobs.generate_current_rate_cache.delay(crc.pk)
        datum['cache'] = crc.pk

    return data


def timeseries(data, outcome_form, filter_form, stratify, chunked, force_cache_update=False):
    from analysis import jobs
    for datum in data:
        trc = TimeseriesResultCache.objects.get_cached_results(outcome_form, filter_form,
                                                               location=datum['location'],
                                                               time_chunk=chunked,
                                                               stratify=stratify)
        if force_cache_update or trc is None or trc.result is None:
            trc = TimeseriesResultCache.objects.cache_results(outcome_form, filter_form, None,
                                                              location=datum['location'],
                                                              time_chunk=chunked,
                                                              stratify=stratify)
            jobs.generate_timeseries_cache.delay(trc.pk)
        datum['cache'] = trc.pk

    return data


def timeseries_trendline(graphable, inflection=None):
    for stratification in graphable:
        stats_input = []
        enum = graphable[stratification]
        for idx, datum in enumerate(enum):
            value = (float(datum['numerator']) / float(datum['denominator'])) * 100.0
            if datum['denominator'] == -1:
                value = None
            stats_input.append(
                {'time': idx,
                 'Reference': value})

        stats = Stats(stats_input, inflection_point=0 if inflection is None else inflection)
        stats.generate_results()

        raw, graphable[stratification], = graphable[stratification], {}
        graphable[stratification]['trend'] = {}
        graphable[stratification]['raw'] = raw
        if inflection is None:
            graphable[stratification]['trend']['points'] = [datum.get('lineReference', 0) for datum in stats.data]
            try:
                graphable[stratification]['trend']['info'] = stats.ref_trend.summary().as_text()
            except AttributeError:
                graphable[stratification]['trend']['info'] = None
        else:
            before = [datum.get('lineReference', 0) for datum in stats.data]
            after = [datum.get('lineReference2', 0) for datum in stats.data]
            graphable[stratification]['trend']['points'] = before[:inflection + 1] + ([None] *
                                                                                      (len(before) - inflection + 1))
            graphable[stratification]['inflect'] = {}
            graphable[stratification]['inflect']['points'] = ([None] * inflection) + after[inflection:]
            try:
                graphable[stratification]['trend']['info'] = stats.ref_trend.summary().as_text()
            except AttributeError:
                graphable[stratification]['trend']['info'] = None
            try:
                graphable[stratification]['inflect']['info'] = stats.ref_inflect.summary().as_text()
            except AttributeError:
                graphable[stratification]['inflect']['info'] = None

    return graphable


def sufficient_data(ratio_dict):
    return census_satisfied({"numerator": sum(item['numerator'] for item in ratio_dict),
                             "denominator": sum(item['denominator'] for item in ratio_dict)})


def filter_by_location(location, model, filter_form):
    if not location == 'Massachusetts':
        try:
            interest_zips = City.objects.get(city=location).zips.values_list('zip', flat=True)
        except City.DoesNotExist:
            interest_zips = [location]

        filtered = filter_form.process(model.objects.filter(census__in=interest_zips))
    else:
        filtered = filter_form.process(model.objects.all())

    return filtered


def census_satisfied(ratio):
    if ratio['numerator'] > 4 and ratio['denominator'] > 99:
        return True

    return False


def set_decimals(ratio):
    if ratio > 1:
        return "{:01.1f}".format(ratio)
    else:
        return "{:01.2f}".format(ratio)


def get_quarter(month):
    return "Q{}".format(math.ceil(month / 3))


def unpack_params(request, param, default):
    if request.get_full_path().split('?')[0] not in request.META['HTTP_REFERER']:
        return default
    if request.method == "POST":
        query = request.POST.get('query', "")
    else:
        query = request.META['QUERY_STRING']
    if query == "":
        return default
    for qs in query.split('&'):
        k, v = qs.split("=")
        if k == param:
            if v == '':
                return default
            return v
    return default


def _graph_view(request, pk1, pk2, model, inflection=None):
    if request.is_ajax():
        if request.method == 'GET':
            pks = [pk1]
            if pk2 != '-1':
                pks.append(pk2)
            data = {}
            for pk in pks:
                rc = get_object_or_404(model, pk=pk)
                if rc.result is not None:
                    if model == TimeseriesResultCache:
                        if inflection is not None:
                            inflection = int(inflection)
                        rc.result = _set_timeseries_bounds(request, rc.result)
                        data[rc.location] = timeseries_trendline(rc.result, inflection=inflection)
                    else:
                        data[rc.location] = rc.result
            if len(data) == len(pks):
                return JsonResponse({"complete": True, "data": data})
            else:
                return JsonResponse({"complete": False})
        else:
            return JsonResponse(status=405)
    else:
        return JsonResponse(status=400)


def _set_timeseries_bounds(request, result):
    sm = request.GET.get('sbm')
    sq = request.GET.get('sbq').replace('Q', '')
    sy = request.GET.get('sby')
    em = request.GET.get('ebm')
    eq = request.GET.get('ebq').replace('Q', '')
    ey = request.GET.get('eby')

    chunked = request.GET.get('chunked')
    try:
        if chunked == "month":
            start = arrow.get(int(sy), int(sm), 1)
            end = arrow.get(int(ey), int(em), 1)
        elif chunked == "year":
            start = arrow.get(int(sy), 1, 1)
            end = arrow.get(int(ey), 1, 1)
        elif chunked == "quarter":
            start = arrow.get(int(sy), ((int(sq) - 1) * 3) + 1, 1)
            end = arrow.get(int(ey), ((int(eq) - 1) * 3) + 1, 1)
    except TypeError:
        return result

    bound_result = {}
    for stratification in result:
        strat = []
        for point in result[stratification]:
            if chunked == "month":
                point_time = arrow.get(point['year'], point['month'], 1)
            elif chunked == "year":
                point_time = arrow.get(point['year'], 1, 1)
            elif chunked == "quarter":
                q = point['quarter']
                point_time = arrow.get(point['year'], ((q-1)*3)+1, 1)
            if start <= point_time <= end:
                strat.append(point)
        bound_result[stratification] = strat

    return bound_result


def get_data_time_range():
    times = TrimtrackerData.objects.values('year', 'month').order_by('year', 'month')
    first, last = times.first(), times.last()
    start, end = arrow.get(first['year'], first['month'], 1), arrow.get(last['year'], last['month'], 1)
    drm = arrow.Arrow.range('month', start, end)
    drq = arrow.Arrow.range('quarter', start, end)
    dry = arrow.Arrow.range('year', start, end)
    return drm, drq, dry, start, end
