# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0009_auto_20161020_1139'),
    ]

    operations = [
        migrations.CreateModel(
            name='TimeseriesResultCache',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('outcome', jsonfield.fields.JSONField(default=dict)),
                ('filter', jsonfield.fields.JSONField(default=dict)),
                ('location', models.CharField(max_length=25)),
                ('time_chunk', models.CharField(max_length=10)),
                ('stratify', models.CharField(max_length=16, null=True, blank=True)),
                ('result', jsonfield.fields.JSONField(default=dict)),
            ],
        ),
        migrations.DeleteModel(
            name='ResultCache',
        ),
        migrations.AlterUniqueTogether(
            name='timeseriesresultcache',
            unique_together=set([('outcome', 'filter', 'location', 'stratify', 'time_chunk')]),
        ),
    ]
