# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0010_auto_20161020_1147'),
    ]

    operations = [
        migrations.AlterField(
            model_name='timeseriesresultcache',
            name='result',
            field=jsonfield.fields.JSONField(null=True, blank=True),
        ),
    ]
