# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0004_resultcache'),
    ]

    operations = [
        migrations.AddField(
            model_name='resultcache',
            name='outcome',
            field=jsonfield.fields.JSONField(default=dict),
        ),
    ]
