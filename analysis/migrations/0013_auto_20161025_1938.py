# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0012_timeseriesresultcache_viewed'),
    ]

    operations = [
        migrations.CreateModel(
            name='MapResultCache',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('outcome', jsonfield.fields.JSONField(default=dict)),
                ('filter', jsonfield.fields.JSONField(default=dict)),
                ('result', jsonfield.fields.JSONField(null=True, blank=True)),
                ('viewed', models.IntegerField(default=1)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='mapresultcache',
            unique_together=set([('outcome', 'filter')]),
        ),
    ]
