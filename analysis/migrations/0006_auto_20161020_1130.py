# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0005_resultcache_outcome'),
    ]

    operations = [
        migrations.AddField(
            model_name='resultcache',
            name='filter',
            field=jsonfield.fields.JSONField(default=dict),
        ),
        migrations.AddField(
            model_name='resultcache',
            name='interface',
            field=models.CharField(default={}, max_length=4, choices=[('Curr', 'current'), ('Map', 'map'), ('Time', 'timeseries')]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='resultcache',
            name='result',
            field=jsonfield.fields.JSONField(default=dict),
        ),
    ]
