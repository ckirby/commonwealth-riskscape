# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analysis', '0014_auto_20161027_0804'),
    ]

    operations = [
        migrations.AddField(
            model_name='summaryoutcome',
            name='chart_type',
            field=models.CharField(default='D', max_length=1, choices=[('D', 'Donut'), ('S', 'Sparkline')]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='summaryoutcome',
            name='filter',
            field=jsonfield.fields.JSONField(default=dict),
        ),
        migrations.AddField(
            model_name='summaryoutcome',
            name='outcome',
            field=jsonfield.fields.JSONField(default=dict),
        ),
    ]
