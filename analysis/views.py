from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.template.response import TemplateResponse

from analysis import jobs
from analysis.models import MapResultCache, CurrentRateResultCache, TimeseriesResultCache
from analysis.view_utils import current_rates, timeseries, _graph_view, unpack_params
from . import forms


@login_required
def home(request):
    outcome_form, filter_form = forms.process_queryform(request)
    return render(request, 'analysis/home.html',
                  {"landing": True, "outcome_form": outcome_form, "filter_form": filter_form})


@login_required
def about(request):
    outcome_form, filter_form = forms.process_queryform(request)
    context = {"about": True}
    if outcome_form is not None:
        if outcome_form.is_valid() and filter_form.is_valid():
            pass
        context['outcome_form'] = outcome_form
        context['filter_form'] = filter_form
    return TemplateResponse(request, 'analysis/about.html', context)


@login_required
def heat_map(request, outcome=None):
    outcome_form, filter_form = forms.process_queryform(request, outcome)
    if outcome_form is None:
        return redirect(reverse('home'))
    if outcome_form.is_valid() and filter_form.is_valid():
        pass
    mrc = MapResultCache.objects.get_cached_results(outcome_form, filter_form)
    if mrc is None or mrc.result is None:
        mrc = MapResultCache.objects.cache_results(outcome_form, filter_form, None)
        jobs.generate_map_cache.delay(mrc.pk)

    return TemplateResponse(request, 'analysis/map.html',
                            {"map": True,
                             "map_cache": mrc.pk,
                             "outcome_form": outcome_form,
                             "filter_form": filter_form})


@login_required
def map_overlay(request, pk):
    if request.is_ajax():
        if request.method == 'GET':
            mrc = get_object_or_404(MapResultCache, pk=pk)
            if mrc.result is not None:
                return JsonResponse({"complete": True, "data": mrc.result})
            else:
                return JsonResponse({"complete": False})
        else:
            return JsonResponse(status=405)
    else:
        return JsonResponse(status=400)


@login_required
def graph_view(request, graph_type, outcome=None):
    outcome_form, filter_form = forms.process_queryform(request, outcome)
    if outcome_form is None:
        return redirect(reverse('home'))
    if outcome_form.is_valid() and filter_form.is_valid():
        pass

    data = [{'location': ''}, {'location': ''}]
    locations = unpack_params(request, 'locations', 'Massachusetts,Massachusetts').split(',')

    data[0]['location'] = locations[0]
    try:
        data[1]['location'] = locations[1]
    except IndexError:
        data[1]['location'] = locations[0]

    stratify = unpack_params(request, 'stratify',
                             'age_group' if graph_type == "current" else 'none'
                             )

    chunked = unpack_params(request, 'chunked', 'month')

    context = {"stratify": stratify, "outcome_form": outcome_form, "filter_form": filter_form}

    sby = unpack_params(request, 'sby', None)
    if sby is not None:
        context['dynamic_start'] = {'quarter': unpack_params(request, 'sbq', None),
                                    'month': unpack_params(request, 'sbm', None),
                                    'year': sby}
        context['dynamic_end'] = {'quarter': unpack_params(request, 'ebq', None),
                                  'month': unpack_params(request, 'ebm', None),
                                  'year': unpack_params(request, 'eby', None)}

    try:
        if graph_type == "current":
            context["current"] = True
            context['data'] = current_rates(data, outcome_form, filter_form, stratify)
            template = 'analysis/current.html'
        elif graph_type == "timeseries":
            context['timeseries'] = True
            context['data'] = timeseries(data, outcome_form, filter_form, stratify, chunked)
            context['chunked'] = chunked

            template = 'analysis/timeseries.html'
        else:
            return redirect(reverse('home'))
    except AttributeError as e:
        return redirect(reverse('home'))

    tr = TemplateResponse(request, template, context)

    return tr


@login_required
def current_graph(request, pk1, pk2):
    return _graph_view(request, pk1, pk2, CurrentRateResultCache)


@login_required
def timeseries_graph(request, pk1, pk2):
    return _graph_view(request, pk1, pk2, TimeseriesResultCache)


@login_required
def timeseries_trend(request, inflection, pk1, pk2):
    return _graph_view(request, pk1, pk2, TimeseriesResultCache, inflection)
