from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from coc.forms import COCForm, UploadCOCForm
from coc.models import UploadCOC, COCDisease
from trimscape import utils


@login_required
def coc_home(request):
    data = []
    for d in COCDisease.objects.all():
        data.append(
            {"display_name": d.name, "name": d.slug, "files": ",".join([f.data_file.url for f in d.files.all()])})

    return render(request, 'coc/home.html',
                  {"coc": True, "outcomes": utils.nest(utils.pad_dashboard(data)),
                   "sites": UploadCOC.objects.filter(coc_disease='hepc')})


@login_required
def coc(request, slug):
    form = COCForm(slug)
    return render(request, 'coc/detail.html',
                  {"coc": True, "form": form, "sites": UploadCOC.objects.filter(coc_disease=slug)})


@csrf_exempt
def process_upload(request):
    if request.method == 'POST':
        form = UploadCOCForm(request.POST, request.FILES)
        if form.is_valid():
            upload_data = form.save()
            return HttpResponse('Upload Successful')
        else:
            return HttpResponse("Errors %s" % form.errors)
    else:
        return HttpResponse(status=403)
