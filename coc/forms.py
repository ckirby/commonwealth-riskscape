import arrow
from django import forms

from coc.models import UploadCOC


class UploadCOCForm(forms.ModelForm):
    class Meta:
        model = UploadCOC
        fields = ['data_file', 'site_name', 'coc_disease']


def get_year_choices(choices):
    return choices + [(str(y + 1), str(y + 1)) for y in range(2009, arrow.now().year)]


def get_site_choices(choices, disease):
    return choices + [(s.site_name, s.get_site_name_display()) for s in UploadCOC.objects.filter(coc_disease=disease)]


class COCForm(forms.Form):
    ATTRS = {'class': 'custom-select form-control small'}
    site = forms.ChoiceField(label='Site',
                             choices=(('x', 'All'),),
                             widget=forms.Select(attrs=ATTRS))
    year = forms.ChoiceField(label='Year', choices=(('YEAR', 'All'),),
                             widget=forms.Select(attrs=ATTRS))
    age_group = forms.ChoiceField(label='Age Group',
                                  choices=(('x', 'All'), ('1', '0-19'), ('2', '20-39'), ('3', '40-59'), ('4', '59+'),
                                           ('5', 'Unknown')),
                                  widget=forms.Select(attrs=ATTRS))
    birth_cohort = forms.ChoiceField(label='Birth Cohort',
                                     choices=(('x', 'All'), ('1', 'Before 1945'), ('2', '1945 to 1965'),
                                              ('3', 'After 1965'), ('4', 'Unknown')),
                                     widget=forms.Select(attrs=ATTRS))
    sex = forms.ChoiceField(label="Sex", choices=(('x', 'All'), ('1', 'Female'), ('2', 'Male'), ('3', 'Unknown')),
                            widget=forms.Select(attrs=ATTRS))
    race_ethnicity = forms.ChoiceField(label="Race/Ethnicity",
                                       choices=(('x', 'All'), ('1', 'Asian'), ('2', 'Black'), ('3', 'Caucasian'),
                                                ('4', 'Hispanic'), ('5', 'Other'), ('6', 'Unknown')),
                                       widget=forms.Select(attrs=ATTRS))

    def __init__(self, disease_slug, *args, **kwargs):
        super(COCForm, self).__init__(*args, **kwargs)
        self.fields['year'].choices = get_year_choices(self.fields['year'].choices)
        self.fields['site'].choices = get_site_choices(self.fields['site'].choices, disease_slug)
