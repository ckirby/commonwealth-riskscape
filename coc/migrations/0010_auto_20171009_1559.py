# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coc', '0009_auto_20171009_1553'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='uploadcoc',
            name='disease',
        ),
        migrations.AlterField(
            model_name='uploadcoc',
            name='coc_disease',
            field=models.ForeignKey(to='coc.COCDisease'),
        ),
    ]
