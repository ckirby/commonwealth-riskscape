# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coc', '0007_auto_20171009_1546'),
    ]

    operations = [
        migrations.AddField(
            model_name='uploadcoc',
            name='coc_disease',
            field=models.ForeignKey(null=True, to='coc.COCDisease'),
        ),
    ]
