# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coc', '0003_auto_20170424_0918'),
    ]

    operations = [
        migrations.AddField(
            model_name='uploadcoc',
            name='disease',
            field=models.CharField(default='HepC', max_length=15, choices=[('HepC', 'Hepatitis C')]),
            preserve_default=False,
        ),
    ]
