# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coc', '0011_auto_20171009_1616'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uploadcoc',
            name='coc_disease',
            field=models.ForeignKey(to='coc.COCDisease', related_name='files'),
        ),
    ]
