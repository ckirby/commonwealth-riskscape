# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import coc.models


class Migration(migrations.Migration):

    dependencies = [
        ('coc', '0012_auto_20171113_1537'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uploadcoc',
            name='data_file',
            field=models.FileField(upload_to=coc.models.path_and_rename, max_length=255),
        ),
    ]
