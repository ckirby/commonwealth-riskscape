# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import coc.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='UploadCOC',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('data_file', models.FileField(upload_to='coc', storage=coc.models.OverwriteStorage, max_length=255)),
                ('site_name', models.CharField(choices=[('ATR', 'Atrius'), ('CHA', 'CHA'), ('ML', 'Mass League')], max_length=3)),
            ],
        ),
    ]
