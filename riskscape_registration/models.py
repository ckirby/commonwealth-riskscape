from django.contrib.auth.models import User
from django.db import models
from registration.supplements.base import RegistrationSupplementBase


class MyRegistrationSupplement(RegistrationSupplementBase):
    full_name = models.CharField("Full Name", max_length=250)
    institution = models.CharField("Institutional Affiliation", max_length=250)

    def __str__(self):
        return "{}".format(self.full_name)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.CharField("Full Name", max_length=250)
    institution = models.CharField("Institutional Affiliation", max_length=250)
