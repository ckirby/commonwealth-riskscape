from django.dispatch import receiver
from django.template.loader import render_to_string
from registration.signals import user_registered
from django.core.mail import send_mail

from riskscape_registration.utils import get_registration_supplement
from trimscape import settings


@receiver(user_registered)
def alert_staff_to_registration(sender, **kwargs):
    user = kwargs.get('user',None)
    if user is not None:
        profile_supplement = get_registration_supplement(user)
        kwargs['full_name'] = profile_supplement.full_name
        kwargs['institution'] = profile_supplement.institution
        subject = "New Riskscape Registration"
        message = render_to_string('riskscape_registration/registration_alert.txt', kwargs)
        html_message = render_to_string('riskscape_registration/registration_alert.html', kwargs)
        mail_from = getattr(settings, 'EMAIL_HOST_USER', '')
        send_mail(subject, message, mail_from, settings.REGISTRATION_ADMIN_EMAILS, html_message=html_message)

