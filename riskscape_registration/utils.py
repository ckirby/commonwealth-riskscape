from registration import supplements

from trimscape import settings


def get_registration_supplement(user):
    mrs = supplements.get_supplement_class(settings.REGISTRATION_SUPPLEMENT_CLASS)
    profile_cache = user._registration_profile_cache
    return mrs.objects.get(registration_profile_id=profile_cache.id)
