from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import redirect, render, get_object_or_404
from django.template.loader import render_to_string
from registration.models import RegistrationProfile
from registration.views import ActivationView

from riskscape_registration.models import Profile
from riskscape_registration.utils import get_registration_supplement
from trimscape import settings


class RiskscapeActivationView(ActivationView):
    def form_valid(self, form):
        profile = self.get_object()
        password = form.cleaned_data['password1']
        self.activated_user = self.backend.activate(
            profile.activation_key, self.request, password=password)
        user = authenticate(username=self.activated_user.username, password=password)
        login(self.request, user)
        return redirect(reverse('home'))


@staff_member_required
def review(request):
    unprocessed = RegistrationProfile.objects.filter(_status='untreated').select_related()
    processed = RegistrationProfile.objects.exclude(_status='untreated').select_related()
    users = User.objects.filter(is_active=True).select_related()
    return render(request, 'riskscape_registration/process.html',
                  {'to_decide': unprocessed, 'processed': processed, 'users': users})


@staff_member_required
def deny(request, pk):
    return _decide(request, pk, False)


@staff_member_required
def approve(request, pk):
    return _decide(request, pk, True)


@staff_member_required
def retry(request, pk):
    return _decide(request, pk, False, notify=True)


@staff_member_required
def _decide(request, pk, should_approve, notify=False):
    if request.is_ajax():
        if request.method == 'POST':
            profile = get_object_or_404(RegistrationProfile, pk=pk)
            if should_approve:
                user = RegistrationProfile.objects.accept_registration(profile, None, False, force=True)
                _send_accept_registration_email(user)
            else:
                user = RegistrationProfile.objects.reject_registration(profile, None, False)
                if user is not None:
                    if notify:
                        _send_retry_registration_email(user)
                    else:
                        _send_registration_status_to_admins(user, "Denied")
                    user.delete()
                    profile.delete()
            return JsonResponse({'completed': pk, 'approved': should_approve})
        else:
            return HttpResponse(status=405)
    else:
        return HttpResponse(status=400)


@staff_member_required
def delete(request, pk):
    if request.is_ajax():
        if request.method == 'POST':
            profile = get_object_or_404(RegistrationProfile, pk=pk)
            profile.user.delete()
            profile.delete()
            return JsonResponse({'completed': pk})
        else:
            return HttpResponse(status=405)
    else:
        return HttpResponse(status=400)


def _send_accept_registration_email(user):
    if user is not None:
        profile_supplement = get_registration_supplement(user)
        user.save()
        profile = Profile()
        profile.full_name = profile_supplement.full_name
        profile.institution = profile_supplement.institution

        profile_cache = user._registration_profile_cache
        subject = "Welcome to Riskscape"
        message = render_to_string('riskscape_registration/registration_activated.txt',
                                   {'full_name': user.first_name,
                                    'activation_key': profile_cache.activation_key,
                                    'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS})
        html_message = render_to_string('riskscape_registration/registration_activated.html',
                                        {'full_name': user.first_name,
                                         'activation_key': profile_cache.activation_key,
                                         'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS})
        mail_from = getattr(settings, 'EMAIL_HOST_USER', '')
        send_mail(subject, message, mail_from, [user.email], html_message=html_message)

        _send_registration_status_to_admins(user, "Accepted")


def _send_retry_registration_email(user):
    if user is not None:
        profile_supplement = get_registration_supplement(user)
        full_name = profile_supplement.full_name
        institution = profile_supplement.institution

        subject = "Riskscape Registration Request"
        message = render_to_string('riskscape_registration/retry_registration.txt',
                                   {'full_name': full_name, 'institution': institution})
        html_message = render_to_string('riskscape_registration/retry_registration.html',
                                        {'full_name': full_name, 'institution': institution})
        mail_from = getattr(settings, 'EMAIL_HOST_USER', '')
        send_mail(subject, message, mail_from, [user.email], html_message=html_message)

        _send_registration_status_to_admins(user, "Deferred")


def _send_registration_status_to_admins(user, status):
    profile_supplement = get_registration_supplement(user)
    full_name = profile_supplement.full_name

    subject = "Riskscape Registration Request {}".format(status)
    message = render_to_string('riskscape_registration/registration_decided.txt',
                               {'full_name': full_name, 'status': status})
    mail_from = getattr(settings, 'EMAIL_HOST_USER', '')
    send_mail(subject, message, mail_from, settings.REGISTRATION_ADMIN_EMAILS)
